Chrome extension that makes MRs easier to review by reducing whitespace and putting important information in a floating panel.

|Toggle-able panel that shows important info about the MR|
|-|
|![panel.png](panel.png)|

|Compact MR info|
|-|
|![compact1.png](compact1.png)|

|Compact comments|
|-|
|![compact2.png](compact2.png)|
