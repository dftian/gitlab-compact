// ---------------------------------------------------------------------------------------------------------------------
// Helper functions
// ---------------------------------------------------------------------------------------------------------------------

// Wait for an element (or elements) to exist before returning a promise.
function waitFor(selector) {
  return new Promise((resolve) => {
    const checkElementExists = () => {
      const element = $(selector)

      if (element.length) resolve(element)
      else requestAnimationFrame(checkElementExists) // Not as performant as MutationObserver, but much easier to use.
    }

    checkElementExists()
  })
}

// ---------------------------------------------------------------------------------------------------------------------
// Panel button
// ---------------------------------------------------------------------------------------------------------------------
// "i" button that opens the panel.
function createPanelButton(panel) {
  // Check if the click was outside the panel, and if so, hide the panel.
  function checkClick(e) {
    if (panel.get(0).contains(e.target)) return
    panel.addClass('d-none')
    panel.trigger('hide')
    window.removeEventListener('click', checkClick)
  }

  return $('<button class="btn btn-primary panel-button">🛈</button>')
    .click(() => {
      panel.removeClass('d-none')
      panel.trigger('show')
      window.addEventListener('click', checkClick)
      return false
    })
}

// ---------------------------------------------------------------------------------------------------------------------
// Branch names
// ---------------------------------------------------------------------------------------------------------------------
async function branchNames(panel) {
  const branch = $(`
    <div class="branch group">
      <label>Branch:</label>
      <a href="#" class="text-truncate">${(await waitFor('.js-source-branch')).text()}</a>
      <div class="mx-1">🠆</div>
      <a href="#" class="text-truncate">${(await waitFor('.js-target-branch')).text()}</a>
    </div>
  `)

  let notification, hideTimeout

  function copyToClipboard(e) {
    // If there's already a "copied to clipboard" notification, remove it.
    if (notification) {
      notification.remove()
      clearTimeout(hideTimeout)
    }

    notification = $('<div class="badge badge-success position-absolute">Copied to clipboard</div>')
      .css('zIndex', 600)
      .hide()
      .appendTo(document.body)

    notification.position({ my: 'bottom-5', at: 'top', of: e.target }) // Position the notification window centered above the branch link.

    // Show the notification and hide it after 1 second.
    notification.show('drop', { direction: 'down' }, 150)
    hideTimeout = setTimeout(() => notification.hide('drop', { direction: 'down' }, 100), 1000)

    navigator.clipboard.writeText(e.target.innerText)
    return false
  }

  // If the panel is hidden, remove the "copied to clipboard" notification if there is one showing.
  panel.on('hide', () => {
    if (notification) {
      notification.remove()
      clearTimeout(hideTimeout)
    }
  })

  branch.find('a').click(copyToClipboard)
  return branch
}

// ---------------------------------------------------------------------------------------------------------------------
// Pipeline status
// ---------------------------------------------------------------------------------------------------------------------
async function pipelineStatus(panel) {
  const status = await waitFor('.mr-widget-pipeline-graph .stage-cell')
  const parent = status.parent()
  const div = $(`
    <div class="group">
      <label>Pipeline:</label>
    </div>
  `)

  // The pipeline graph doesn't work properly if cloned. Instead, we'll just move it into the panel when the panel is
  // shown, then move it back to its original location when the panel is hidden.
  panel.on('show', () => div.append(status))
  panel.on('hide', () => parent.append(status))

  return div
}

async function layoutButtons() {
  const div = $(`
    <div class="group">
      <label>Layout:</label>
      <div class="btn-group"></div>
    </div>
  `)

  function createButton(text, buttonGroup, isCompact) {
    const activeClass = 'btn-info active'
    const button = $(`<button class="btn">${text}</button>`)
      .toggleClass(activeClass, isCompact)
      .appendTo(buttonGroup)
      .click(() => {
        buttonGroup.find('.active').removeClass(activeClass)
        button.addClass(activeClass)
        content.toggleClass('compact', isCompact)
      })
  }

  const content = (await waitFor('.issuable-discussion')).addClass('compact')
  const buttonGroup = div.find('.btn-group')
  createButton('Compact', buttonGroup, true)
  createButton('Original', buttonGroup, false)

  return div
}

async function activityButtons() {
  const originalNewest = await waitFor('.js-newest-first')
  const originalOldest = await waitFor('.js-oldest-first')
  const originalAll = await waitFor('.js-discussion-filter-container [data-filter-type="all"] button')
  const originalComments = await waitFor('.js-discussion-filter-container [data-filter-type="comments"] button')
  const originalHistory = await waitFor('.js-discussion-filter-container [data-filter-type="history"] button')

  const div = $(`
    <div class="group">
      <label>Activity:</label>
    </div>
  `)

  function createButton(text, originalButton, buttonGroup) {
    const activeClass = 'btn-warning active'
    const button = $(`<button class="btn">${text}</button>`)
      .toggleClass(activeClass, originalButton.hasClass('is-active'))
      .click(() => originalButton.click())
      .appendTo(buttonGroup)

    originalButton.on('click', (e) => {
      buttonGroup.find('.active').removeClass(activeClass)
      button.addClass(activeClass)
      // Prevent the panel from closing if the button in the panel was clicked. If there's an original event, the click
      // on the original button was by a user. Otherwise, it was triggered by JS through the click on the panel button.
      if (!e.originalEvent) e.stopPropagation()
    })
  }

  const groupOne = $('<div class="btn-group"></div>').appendTo(div)
  createButton('Newest first', originalNewest, groupOne)
  createButton('Oldest first', originalOldest, groupOne)

  const groupTwo = $('<div class="btn-group"></div>').appendTo(div)
  createButton('All', originalAll, groupTwo)
  createButton('Comments only', originalComments, groupTwo)
  createButton('History only', originalHistory, groupTwo)

  return div
}

// ---------------------------------------------------------------------------------------------------------------------
// GitLab Bot Tables
// ---------------------------------------------------------------------------------------------------------------------
async function gitlabBotTables() {
  const botComments = await waitFor('[data-username="gitlab-bot"]')
  const tables = botComments.closest('.timeline-content').find('table')
  return tables.clone()
}

// ---------------------------------------------------------------------------------------------------------------------
// Panel
// ---------------------------------------------------------------------------------------------------------------------
async function createPanel() {
  const panel = $(`
    <div class="border bg-white md shadow-lg panel d-none">
      <div class="bg-primary d-flex panel-header"></div>
      <div class="px-3 py-2 body"></div>
    </div>
  `)

  const panelBody = panel.find('.body')

  panelBody.append(await branchNames(panel))
  panelBody.append(await pipelineStatus(panel))
  panelBody.append(await layoutButtons())
  panelBody.append(await activityButtons())
  panelBody.append(await gitlabBotTables())

  return panel
}

// ---------------------------------------------------------------------------------------------------------------------
// Entry function
// ---------------------------------------------------------------------------------------------------------------------
(async () => {
  const navbar = await waitFor('.nav-sidebar')
  const panel = await createPanel()

  navbar.append(createPanelButton(panel))
  navbar.append(panel)
})()
